# README #

### What is this repository for? ###

* A demonstration of Peer to Peer Networking

### How do I get set up? ###

* Requires XNA and .NET 4.0
* You may be able to use the following to set up XNA on Windows 8 and VS 2012+: http://msxna.codeplex.com/

### Who do I talk to? ###

* Paul Boocock (paul.boocock@staffs.ac.uk)
* Cathy French (c.l.french@staffs.ac.uk)